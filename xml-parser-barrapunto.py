#!/usr/bin/python


# Vamos a parsear el fichero jokes.xml

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import urllib.request


class CounterHandler(ContentHandler):

    def __init__(self):
        self.inContent = 0
        self.theContent = ""
        self.inItem = 0
        self.title = ""
        self.link = ""

    def startElement(self, name, attrs):
        if name == "item":
            self.inItem = 1
        elif self.inItem:
            if name == "title":
                self.inContent = 1
            elif name == "link":
                self.inContent = 1

    def endElement(self, name):
        if self.inItem:
            if name == "item":
                self.inItem = 0
                print('<p><a href=' + self.link + '>' + self.title + '</a></p>')
            elif name == "title":
                self.title = self.theContent
                print("<html><body> Titulo: " + self.title + "</body></html>")
                print("Titulo :" + self.title)
            elif name == "link":
                self.link = self.theContent
                print("<html><body> Link : " + self.link + "</body></html>")
                print("Link :" + self.link)

            if self.inContent:
                self.inContent = 0
                self.theContent = ""

    def characters(self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars


# Main program
if len(sys.argv) < 2:
    print("Usage: python xml-parser-barrapunto.py <document>")
    print(" <document>: file name of the document to parse")
    sys.exit(1)

# Load parser and driver

Parser = make_parser()
Handler = CounterHandler()
Parser.setContentHandler(Handler)

# Ready, set, go!

xmlFile = urllib.request.urlopen("http://barrapunto.com/index.rss")
Parser.parse(xmlFile)

print("Parse complete")
